import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType
import org.apache.spark.streaming.Minutes

object g3q2 {
  def main(args: Array[String]): Unit = {
    val reader = common.getReader(args(0) + "/*2008*", args(1), "16G", Minutes(10))
    val leg1 = reader
      .filter(col("depTime") < 1200)
      .toDF(common.createSchemaArray("1").map(_.name): _*)
      .drop("depDelay1", "flightNum1")
    val leg2 = reader
      .filter(col("depTime") > 1200)
      .toDF(common.createSchemaArray("2").map(_.name): _*)
      .drop("depDelay2", "flightNum2")

    val dataFrame = leg1
      .join(leg2,
        col("dest1") === col("origin2")
          && datediff(col("date2"), col("date1")).equalTo(2))
      // Can't add Date to DynamoDB
      .withColumn("date1", col("date1").cast(StringType))
      .withColumn("date2", col("date2").cast(StringType))
      .withColumn("totalArrDelay", col("arrDelay1") + col("arrDelay2"))
      .withColumn(
        "dayRouteComposite",
        concat_ws(
          " ",
          array(col("date1"), col("origin1"), col("dest1"), col("dest2"))))
      .withColumn(
        "flightNumComposite",
        concat_ws("-", array(
          // All these are required, otherwise we get duplicate key exceptions
          col("depTime1"),
          col("carrier1"),
          col("depTime2"),
          col("carrier2"))))
        .dropDuplicates("dayRouteComposite", "flightNumComposite")
    common.runStreamingJob(args(2), dataFrame, "append")
  }
}