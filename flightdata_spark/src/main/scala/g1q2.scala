import org.apache.spark.sql.functions._

object g1q2 {
  def main(args: Array[String]): Unit = {
    val reader = common.getReader(args(0), args(1))

    val dataFrame = reader
      .select("carrier", "arrDelay")
      .groupBy("carrier")
      .agg(avg("arrDelay"))
      .toDF("carrier", "avgArrDelay")

    common.runStreamingJob(args(2), dataFrame)
  }
}
