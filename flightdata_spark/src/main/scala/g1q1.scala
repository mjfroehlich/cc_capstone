import org.apache.spark.sql.functions._

object g1q1 {

  def main(args: Array[String]): Unit = {
    val reader = common.getReader(args(0), args(1))

    val dataFrame = reader
      .select("origin")
      .toDF("airport")
      .union(reader
        .select("dest")
        .toDF("airport"))
      .groupBy("airport")
      .agg(count("airport"))
      .toDF("airport", "numberFlights")

    common.runStreamingJob(args(2), dataFrame)
  }
}
