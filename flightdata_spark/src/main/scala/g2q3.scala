import org.apache.spark.sql.functions._

object g2q3 {
  def main(args: Array[String]): Unit = {
    val reader = common.getReader(args(0), args(1))

    val dataFrame = reader
      .withColumn(
        "originDest",
        concat(col("origin"), lit("-"), col("dest")))
      .select("originDest", "carrier", "arrDelay")
      .groupBy("originDest", "carrier")
      .agg(avg("arrDelay"))
      .toDF("originDest", "carrier", "avgArrDelay")

    common.runStreamingJob(args(2), dataFrame)
  }
}
