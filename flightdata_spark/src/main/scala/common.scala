import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types._
import com.audienceproject.spark.dynamodb.implicits._
import org.apache.log4j.{LogManager, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Duration, Seconds, StreamingContext}

object common {
  val log: Logger = LogManager.getRootLogger

  def getReader(dataIn: String, dataTmp: String, executorMemory: String = "1G", batchInterval: Duration = Seconds(5)): DataFrame = {
    val conf = new SparkConf()
      .setAppName("flightData")
      .set("spark.executor.memory", executorMemory)
      .set("spark.sql.streaming.checkpointLocation", dataTmp)
      .set("spark.streaming.stopGracefullyOnShutdown", "true")

    val ssc = new StreamingContext(conf, batchInterval)
    ssc.sparkContext.setLogLevel("WARN")

    SparkSession
      .builder
      .appName("flightData")
      .config(ssc.sparkContext.getConf)
      .getOrCreate()
      .readStream
      .option("header", "false")
      .option("delimiter", ";")
      .option("checkpointLocation", dataTmp)
      .schema(new StructType(createSchemaArray()))
      .csv(dataIn)
  }

  def createSchemaArray(suffix: String = ""): Array[StructField] = {
    Array(
      StructField("date" + suffix, DateType, nullable=false),
      StructField("depTime" + suffix, IntegerType, nullable=false),
      StructField("origin" + suffix, StringType, nullable=false),
      StructField("dest" + suffix, StringType, nullable=false),
      StructField("carrier" + suffix, StringType, nullable=false),
      StructField("arrDelay" + suffix, FloatType, nullable=false),
      StructField("flightNum" + suffix, StringType, nullable=false),
      StructField("depDelay" + suffix, FloatType, nullable=false)
    )
  }

  def runStreamingJob(dynamo_table: String, dataFrame: DataFrame, outputMode: String = "complete"): Unit = {
    val writer = dataFrame
      .writeStream
      .format("console")
      .outputMode(outputMode)

    val start = System.currentTimeMillis()
    val streamingQuery = writer.start()

    // We initialize to true, so that we don't exit during setup
    var writeInProgress = true

    writer.foreachBatch({ (batchDF: DataFrame, _: Long) =>
      val start = System.currentTimeMillis()
      writeInProgress = true
      batchDF.write.dynamodb(dynamo_table)
      writeInProgress = false
      logLastProgress(streamingQuery, start)
    }).start()

    while (true) {
      if (!writeInProgress && !streamingQuery.status.isDataAvailable && !streamingQuery.status.isTriggerActive) {
        log.warn("All done. Stopping")
        streamingQuery.sparkSession.sparkContext.stop()
        log.warn("Job took " + (System.currentTimeMillis() - start) / 1000 + "s")
        System.exit(0)
      }

      Thread.sleep(10000)
    }
  }

  def logLastProgress(streamingQuery: StreamingQuery, start: Long): Unit = {
    val so = streamingQuery.lastProgress.stateOperators(0)
    log.warn("numRowsTotal: " + so.numRowsTotal
      + ", numRowsUpdated: " + so.numRowsUpdated
      + ", took " + ((System.currentTimeMillis() - start) / 1000) + "s")
  }
}
