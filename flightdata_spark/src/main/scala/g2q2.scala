import org.apache.spark.sql.functions._

object g2q2 {
  def main(args: Array[String]): Unit = {
    val reader = common.getReader(args(0), args(1))

    val dataFrame = reader
      .select("origin", "dest", "depDelay")
      .groupBy("origin", "dest")
      .agg(avg("depDelay"))
      .toDF("origin", "dest", "avgDepDelay")

    common.runStreamingJob(args(2), dataFrame)
  }
}
