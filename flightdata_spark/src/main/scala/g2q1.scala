import org.apache.spark.sql.functions._

object g2q1 {
  def main(args: Array[String]): Unit = {
    val reader = common.getReader(args(0), args(1))

    val dataFrame = reader
      .select("origin", "carrier", "depDelay")
      .groupBy("origin", "carrier")
      .agg(avg("depDelay"))
      .toDF("origin", "carrier", "avgDepDelay")

    common.runStreamingJob(args(2), dataFrame)
  }
}
