#!/usr/bin/env bash
source common.sh
source aws.sh
source spark.sh

function run_all() {
  export SKIP_ORCHESTRATION=
  g1q1
  export SKIP_ORCHESTRATION=true
  g1q2
  g2q1
  g2q2
  g2q3
  g3q2
}

function results_all() {
  g1q1_results
  g1q2_results
  g2q1_results
  g2q2_results
  g2q3_results
  g3q2_results
}
function g1q1() {
  _run_task g1q1 5 airport S
}

function g1q1_results() {
  echo -e "\nGROUP 1, QUESTION 1"
  aws dynamodb scan \
    --table-name g1q1_${ENV} \
    --query 'Items[*].{AIRPORT:airport.S,NumFlights:numberFlights.N}' \
    --output text \
  | sort -t$'\t' -nrk2 \
  | head
}

function g1q2() {
  _run_task g1q2 10 carrier S
}

function g1q2_results() {
  echo -e "\nGROUP 1, QUESTION 2"
  aws dynamodb scan \
    --table-name g1q2_${ENV} \
    --query 'Items[*].{A_CARRIER:carrier.S,B_DELAY:avgArrDelay.N}' \
    --output text \
  | sort -t$'\t' -nk2 \
  | head
}

function g2q1() {
  _run_task g2q1 10 origin S carrier S
}

function g2q1_query() {
  local origin_value="$1"
  _dynamo_query_by_key \
    g2q1_${ENV} \
    origin ${origin_value} \
    'Items[*].{A_CARRIER:carrier.S,B_DELAY:avgDepDelay.N}' \
  | sort -t$'\t' -nk2 \
  | head
}

function g2q1_results() {
  echo -e "\nGROUP 2, QUESTION 1"
  for originDest in SRQ CMH JFK SEA BOS; do
  echo -e "\nAirport: ${originDest}"
  g2q1_query "${originDest}"
done
}

function g2q2() {
  _run_task g2q2 50 origin S dest S
}

function g2q2_query() {
  local origin_value="$1"
  _dynamo_query_by_key \
    g2q2_${ENV} \
    origin \
    ${origin_value} \
    'Items[*].{A_DEST:dest.S,B_DELAY:avgDepDelay.N}' \
  | sort -t$'\t' -nk2 \
  | head
}

function g2q2_results() {
  echo -e "\nGROUP 2, QUESTION 2"
  for originDest in SRQ CMH JFK SEA BOS; do
  echo -e "\nAirport: ${originDest}"
  g2q2_query "${originDest}"
done
}

function g2q3() {
  _run_task g2q3 50 originDest S carrier S
}

function g2q3_query() {
  _dynamo_query_by_key \
    g2q3_${ENV} \
    originDest \
    "$1" \
    'Items[*].{A_CARRIER:carrier.S,B_DELAY:avgArrDelay.N}' \
  | sort -t$'\t' -nk2 \
  | head
}

function g2q3_results() {
  echo -e "\nGROUP 2, QUESTION 3"
  for originDest in LGA-BOS BOS-LGA OKC-DFW MSP-ATL; do
    echo -e "\nOrigin-destination pair: ${originDest}"
    g2q3_query "${originDest}"
  done
}

function g3q2() {
  _run_task g3q2 400 dayRouteComposite S flightNumComposite S
}

function g3q2_query() {
  _dynamo_query_by_key \
    g3q2_${ENV} \
    dayRouteComposite \
    "$1" \
    'Items[*].{A_TotalArrDelay:totalArrDelay.N,B1_Origin:origin1.S,B2_Dest:dest1.S,B3_DepTime:depTime1.N,B4_ArrDelay:arrDelay1.N,C1_Origin:origin2.S,C2_Dest:dest2.S,C3_DepTime:depTime2.N,C4_ArrDelay:arrDelay2.N}' \
    '--output table'
}

function g3q2_results() {
  echo -e "\nGROUP 3, QUESTION 2"
  for dayRouteComposite in \
    '2008-04-03 BOS ATL LAX' \
    '2008-09-07 PHX JFK MSP' \
    '2008-01-24 DFW STL ORD' \
    '2008-05-16 LAX MIA LAX'
  do
    echo -e "\nDay and Route: ${dayRouteComposite}"
    g3q2_query "${dayRouteComposite}"
  done
}

function _run_task() {
  local class_name="$1"; shift
  local dynamo_write_throughput="$1"; shift
  local dynamo_keys="$@"
  local dynamo_name="${class_name}_${ENV}"

  local hdfs_root="/user/$(whoami)/${class_name}"
  dynamo_create_table ${dynamo_name} ${dynamo_write_throughput} ${dynamo_keys} && \
  spark_run ${class_name} ${dynamo_name}
  aws dynamodb update-table \
    --table-name ${dynamo_name} \
    --provisioned-throughput ReadCapacityUnits=3,WriteCapacityUnits=1
}
