#!/usr/bin/env bash
source common.sh

# export AWS_DEFAULT_PROFILE=ccc

CLUSTER_NAME=ccc
KEY_NAME=${CLUSTER_NAME}
SG_MASTER=sg-069be2c475cc24c68  # AWS-generated EMR master security group
SG_CONTROL_HOST=sg-01dde71f9e00fe2ee  # allows SSH from world
EBS_SNAPSHOT_ID=snap-e1608d88
REGION=us-east-1

INSTANCE_TYPE=m4.xlarge
WORKER_INSTANCE_COUNT=2
VOLUME_SIZE=200

export EMR_USER=hadoop
export EMR_HOME=/home/${EMR_USER}
data_mountpoint="${EMR_HOME}/airline-data"
export EMR_DATA_ROOT="/${data_mountpoint}/aviation/airline_ontime"
export EMR_REPO_ROOT="${EMR_HOME}/cc_capstone"

function ccc_create_cluster() {
  aws emr create-default-roles --output json >&2
  local existing=$(get_cluster_id)
  if [[ ${existing} != "None" ]]; then
    return 0
  fi
  echo $(aws emr create-cluster \
    --applications Name=Hadoop Name=Spark \
    --ec2-attributes KeyName=${KEY_NAME} \
    --use-default-roles \
    --enable-debugging \
    --release-label emr-5.21.0 --log-uri 's3://aws-logs-122367035062-us-east-1/elasticmapreduce/' \
    --name ${CLUSTER_NAME} \
    --scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
    --region ${REGION} \
    --instance-groups "[
        {
          \"InstanceCount\":${WORKER_INSTANCE_COUNT},
          \"EbsConfiguration\":{
            \"EbsBlockDeviceConfigs\":[
              {
                \"VolumeSpecification\":{
                  \"SizeInGB\":${VOLUME_SIZE},\"VolumeType\":\"gp2\"
                },\"VolumesPerInstance\":1
              }
            ],
            \"EbsOptimized\":true
          },
          \"InstanceGroupType\":\"CORE\",
          \"InstanceType\":\"${INSTANCE_TYPE}\",
          \"Name\":\"Core - 2\"
        },
        {
          \"InstanceCount\":1,
          \"EbsConfiguration\":{
            \"EbsBlockDeviceConfigs\":[
              {
                \"VolumeSpecification\":{
                  \"SizeInGB\":100,\"VolumeType\":\"gp2\"
                },\"VolumesPerInstance\":1
              }
            ],\"EbsOptimized\":true
          },
          \"InstanceGroupType\":\"MASTER\",
          \"InstanceType\":\"${INSTANCE_TYPE}\",
          \"Name\":\"Master - 1\"
        }
      ]" \
    --output text --query 'ClusterId')
}

function emr_configure_cluster() {
  local git_repo="https://mjfroehlich@bitbucket.org/mjfroehlich/cc_capstone.git"
  ccc_create_cluster >&2
  local cluster_id=$(get_cluster_id)
  aws emr wait cluster-running --cluster-id ${cluster_id} >&2
  local master_group_id=$(
    aws emr describe-cluster \
      --cluster-id ${cluster_id} \
      --query "Cluster.InstanceGroups[?InstanceGroupType == 'MASTER'].Id | [0]" --output text)
  local master_node_ec2_instance_id=$(
    aws emr list-instances \
      --cluster-id ${cluster_id} \
      --query "Instances[?InstanceGroupId == '${master_group_id}'].Ec2InstanceId | [0]" --output text)
  local master_node_data=(
    $(aws ec2 describe-instances \
      --instance-ids ${master_node_ec2_instance_id} \
      --query "Reservations[0].Instances[0].[Placement.AvailabilityZone,PublicIpAddress,PrivateIpAddress]" --output text))
  local az="${master_node_data[0]}"
  local master_node_ip="${master_node_data[1]}"
  local master_node_private_ip="${master_node_data[2]}"
  local conn_string="${EMR_USER}@${master_node_ip}"

  if [[ -n ${SKIP_ORCHESTRATION} ]]; then
    echo "${master_node_ip} ${master_node_private_ip}"
  else
    aws ec2 modify-instance-attribute \
      --instance-id ${master_node_ec2_instance_id} \
      --groups ${SG_MASTER} ${SG_CONTROL_HOST} >&2


    local vol_id=$(aws ec2 create-volume \
      --snapshot-id ${EBS_SNAPSHOT_ID} --availability-zone ${az} \
      --query VolumeId --output text)
    aws ec2 wait volume-available --volume-ids ${vol_id} >&2

    local device_letter=f
    local original_device_name=/dev/sd${device_letter}
    local translated_device_name=/dev/xvd${device_letter}
    aws ec2 attach-volume \
      --device ${original_device_name} \
      --instance-id ${master_node_ec2_instance_id} \
      --volume-id ${vol_id} >&2

    ssh ${SSH_OPTS} ${conn_string} "sudo yum install -y git \
                  && git clone ${git_repo} ${EMR_REPO_ROOT} || (cd ${EMR_REPO_ROOT}; git pull) \
                  && curl -O https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py --user \
                  && ${EMR_HOME}/.local/bin/pip3 install --user -r ${EMR_REPO_ROOT}/requirements.txt \\
                  && mkdir -pv ${data_mountpoint} \
                  && sudo mount -v ${translated_device_name} ${data_mountpoint}" >&2
    echo "${master_node_ip} ${master_node_private_ip}"
  fi
}

function ccc_emr_terminate_cluster() {
  aws emr terminate-clusters --cluster-ids $(get_cluster_id)
}

function get_cluster_id() {
  echo $(
    aws emr list-clusters --active \
        --query "Clusters[?Status.State != 'TERMINATING'].Id | [0]" --output text)
}

function dynamo_create_table() {
  local name="$1"
  local write_throughput="$2"
  local partition_key_name="$3"
  local partition_key_type="$4"
  local range_key_name="$5"
  local range_key_type="$6"

  local attr_defs=
  local key_schema=
  if [[ "$#" = 4 ]]; then
    attr_defs="AttributeName=${partition_key_name},AttributeType=${partition_key_type}"
    key_schema="AttributeName=${partition_key_name},KeyType=HASH"
  elif [[ "$#" = 6 ]]; then
    attr_defs="AttributeName=${partition_key_name},AttributeType=${partition_key_type}
               AttributeName=${range_key_name},AttributeType=${range_key_type}"
    key_schema="AttributeName=${partition_key_name},KeyType=HASH
                AttributeName=${range_key_name},KeyType=RANGE"
  fi

  local existing_table=$(aws dynamodb describe-table --table-name ${name} 2>/dev/null)
  if [[ -n ${existing_table} ]]; then
    aws dynamodb delete-table --table-name ${name}
    aws dynamodb wait table-not-exists --table-name ${name}
  fi
  aws dynamodb create-table \
    --table-name ${name} \
    --attribute-definitions ${attr_defs} \
    --key-schema ${key_schema} \
    --provisioned-throughput ReadCapacityUnits=3,WriteCapacityUnits=${write_throughput}
  aws dynamodb wait table-exists --table-name ${name}
  echo "Table '${name}' created and active"
}

function _dynamo_query_by_key() {
  local table_name="$1"
  local key="$2"
  local value="$3"
  local query="$4"
  local options="${5:---output text}"

  if ! validate_env; then return; fi

  local scan_option="--scan-index-forward"

  aws dynamodb query \
    --table-name ${table_name} \
    --key-condition-expression "${key} = :name" \
    --expression-attribute-values "{\":name\":{\"S\":\"${value}\"}}" \
    --query ${query} \
    ${options}
}

