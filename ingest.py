from concurrent.futures import ThreadPoolExecutor
import csv
import glob
import os
import shutil
from subprocess import PIPE, Popen
import tempfile
from typing import Optional, List
import zipfile

import click


@click.command()
@click.argument('local_root')
@click.argument('hdfs_in')
@click.option('--write-header', is_flag=True, default=False)
@click.argument('output_columns', nargs=-1)
def ingest(local_root, hdfs_in, output_columns, write_header=False):
    ingester = Ingester(local_root, hdfs_in, write_header, output_columns, int(os.environ.get('MAX_FILES', 0)))
    ingester.ingest()


class Ingester:
    open_attrs = dict(encoding='utf8', errors='ignore')

    def __init__(self, local_root: str, hdfs_in: str, write_header: bool, output_columns: List[str], max_files: int):
        self.local_root = local_root
        self.hdfs_in = hdfs_in
        self.write_header = write_header
        self.output_columns = output_columns
        self.max_files = max_files

        self.added = 0
        self.skipped = 0

    def ingest(self):
        processed_count = 0
        run_shell_cmd(f"hadoop fs -mkdir -p {self.hdfs_in}")
        already_existing = [os.path.splitext(os.path.basename(p.decode('utf8')))[0]
                            for p in run_shell_cmd(f"hadoop fs -ls -C {self.hdfs_in}").split()]
        with ThreadPoolExecutor(max_workers=5) as executor:
            for path in glob.glob(os.path.join(self.local_root, '**'), recursive=True):
                if os.path.splitext(os.path.basename(path))[0] in already_existing:
                    print(f"Skipping {path}. Target file already exists.")
                    continue
                if not os.path.isfile(path):
                    print(f"Skipping {path}. Not a file.")
                    continue
                if self.max_files and processed_count > self.max_files:
                    print(f"Exiting after {self.max_files} files")
                    return
                executor.submit(self.process_path, path)
                processed_count += 1
        print(f"Added: {self.added}; skipped: {self.skipped}")

    def process_path(self, path):
        if zipfile.is_zipfile(path):
            self.ingest_zip(path)
        elif os.path.isfile(path):
            self.ingest_csv(path)

    def ingest_csv(self, path):
        file_name = os.path.basename(path)
        tmp_dir = tempfile.mkdtemp()
        try:
            processed = self.process_csv(path, tmp_dir)
            run_shell_cmd(
                f"hadoop fs -put {processed} {self.hdfs_in}/{file_name}")
            print(f"Processed file {path}")
        except Exception as exc:
            print(f"Skipping file {path}: {exc}")
            pass

        shutil.rmtree(tmp_dir)

    def ingest_zip(self, path):
        print(f"Processing {path}")
        f = zipfile.ZipFile(path, 'r')
        tmp_dir = tempfile.mkdtemp(os.path.basename(path))
        f.extractall(tmp_dir)
        for csv_path in [p for p in glob.glob(f"{tmp_dir}/*") if p.endswith('.csv')]:
            self.ingest_csv(csv_path)
        shutil.rmtree(tmp_dir)

    def process_csv(self, source_path, tmp_dir) -> Optional[str]:
        target_path = os.path.join(tmp_dir, os.path.basename(source_path))
        with open(source_path, **self.open_attrs) as s:
            r = csv.DictReader(s)
            with open(target_path, 'w', **self.open_attrs) as t:
                w = csv.DictWriter(t,
                                   fieldnames=self.output_columns,
                                   quoting=csv.QUOTE_NONE,
                                   delimiter=';')
                if self.write_header:
                    w.writeheader()
                for row in r:
                    rowdict = {k: row[k] for k in self.output_columns}
                    w.writerow(rowdict)
                    self.added += 1
        return target_path


def run_shell_cmd(command):
    print(f"running `{command}`")
    p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, close_fds=True)
    if p.returncode:
        raise Exception(p.returncode)
    stdout = p.stdout.read()
    return stdout


if __name__ == '__main__':
    ingest()
