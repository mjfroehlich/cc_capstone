#!/usr/bin/env bash

KEY_PATH=~/.ssh/ccc.pem
SSH_OPTS="-o StrictHostKeyChecking=no -i ${KEY_PATH}"
PYTHON3="python3"


# Data ingest, local to HDFS
function ingest() {
  local ingest_columns="FlightDate CRSDepTime Origin Dest UniqueCarrier ArrDelay FlightNum DepDelay"
  if [[ -n ${SKIP_INGEST} ]]; then return; fi
  local local_data_root="$1"; shift
  local hdfs_in="$1"; shift
  local columns="$@"

  local start=$(date +%s)

  hadoop fs -mkdir -p "${hdfs_in}" && \
  ${PYTHON3} ingest.py ${local_data_root} ${hdfs_in} ${ingest_columns} && \
  (
    let duration=$(date +%s)-${start} && \
    echo "Data ingest took ${duration} seconds"
  )
}

function validate_env() {
  if ! [[ "${ENV}" =~ ^(test|remote|cluster)$ ]]; then
    echo "Please set ENV envvar to test, remote or cluster"
    return 1
  fi
  return 0
}