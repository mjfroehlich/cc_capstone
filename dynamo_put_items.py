import glob
from decimal import Decimal
import json
import os
from time import sleep
from typing import List, Dict

import boto3
from botocore.exceptions import ClientError

import click

BATCH_SIZE = 25
DEFAULT_BURST_WRITE_CAPACITY = 50


@click.command()
@click.argument('input_format')
@click.argument('root_path')
@click.argument('table_name')
@click.argument('write_capacity', default=DEFAULT_BURST_WRITE_CAPACITY, type=int)
@click.argument('columns', nargs=-1)
def run(input_format, root_path, table_name, write_capacity, columns):
    if input_format == 'csv':
        cl = DynamoDbCsvClient(root_path, table_name, write_capacity, columns)
    elif input_format == 'json':
        assert len(columns) == 0, "Specified both column names and json input format, which makes no sense. Aborting"
        cl = DynamoDbJsonClient(root_path, table_name, write_capacity)
    else:
        raise Exception(f"Format {input_format} not supported")
    cl.put_items()


class DynamoDbAbstractClient:
    def __init__(self, root_path: str, table_name: str, write_capacity):
        self.root_path = root_path
        self.table_name = table_name
        self.write_capacity = write_capacity

        self.dynamo_db = boto3.resource('dynamodb')
        self.table = self.dynamo_db.Table(table_name)
        self.key_columns = {i['AttributeName']: i['AttributeType'] for i in self.table.attribute_definitions}
        self.number_keys = [i for i in self.key_columns if self.key_columns[i] == 'N']
        self.key_column_names = [i['AttributeName'] for i in self.table.attribute_definitions]
        self.original_provisioned_read_throughput = self.table.provisioned_throughput['ReadCapacityUnits']
        self.original_provisioned_write_throughput = self.table.provisioned_throughput['WriteCapacityUnits']
        self.items_added = 0

    def dict_from_line(self, line: str) -> Dict:
        return NotImplementedError()

    def put_items(self):
        try:
            self.update_provisioned_throughput(self.original_provisioned_read_throughput,
                                               self.write_capacity)
            for match in glob.glob(os.path.join(self.root_path, '**'), recursive=True):
                if os.path.isfile(match) and os.path.getsize(match):
                    self.put_items_from_file(match)
        finally:
            self.update_provisioned_throughput(self.original_provisioned_read_throughput,
                                               self.original_provisioned_write_throughput)

    def put_items_from_file(self, path: str):
        with open(path, 'r') as f:
            with self.table.batch_writer(overwrite_by_pkeys=self.key_column_names) as batch:
                for line in f:
                    batch.put_item(self.create_request_item(line))
                    self.items_added += 1
        print(f"{self.items_added}.", end='', flush=True)

    def create_request_item(self, line: str):
        row_dict = self.dict_from_line(line)
        for k in self.number_keys:
            row_dict[k] = Decimal(row_dict[k])
        return row_dict

    def update_provisioned_throughput(self, read: int, write: int) -> None:
        retry_interval = 10
        while True:
            try:
                self.table.update(
                    TableName=self.table_name,
                    ProvisionedThroughput={'ReadCapacityUnits': read, 'WriteCapacityUnits': write}
                )
                return
            except ClientError as exc:
                msg = exc.response['Error']['Message']
                print(msg)
                if 'The requested value equals the current value' in msg:
                    print('No change.')
                    return
                elif (
                        'Attempt to change a resource which is still in use' in msg
                        or 'Provisioned throughput decreases are limited within a given UTC day' in msg
                ):
                    print('Retrying.')
                    sleep(retry_interval)
                    retry_interval *= 2
                else:
                    raise exc


class DynamoDbCsvClient(DynamoDbAbstractClient):
    def __init__(self, root_path: str, table_name: str, write_capacity: int, columns: List[str]):
        self.columns = columns
        super().__init__(root_path, table_name, write_capacity)

        self.columns_not_in_keys = [c for c in self.columns if c not in self.key_columns]

    def dict_from_line(self, line):
        return dict(zip(self.columns, line.strip().split('\t')))


class DynamoDbJsonClient(DynamoDbAbstractClient):
    def dict_from_line(self, line: str):
        return {k: str(v) for (k, v) in json.loads(line).items()}


if __name__ == '__main__':
    run()
