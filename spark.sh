#!/usr/bin/env bash
source aws.sh
source common.sh

SCALA_VERSION=2.11
VERSION=0.1
SCALA_PKG_NAME=flightdata_spark
SCALA_PKG_ROOT=${SCALA_PKG_NAME}/
SPARK_JAR_PATH=${SCALA_PKG_ROOT}/target/scala-${SCALA_VERSION}/${SCALA_PKG_NAME}_${SCALA_VERSION}-${VERSION}.jar
SPARK_EMR_JAR_PATH=${EMR_HOME}/flightdata_spark.jar


function spark_run() {
  if ! validate_env; then return 1; fi
  spark_run_${ENV} $@
}

function spark_run_test() {
  local class_name="$1"
  local dynamo_name="$2"

  local local_data_root=${TEST_DATA_ROOT:-test/data/airline_ontime}
  local hdfs_data_root=${HDFS_DATA_ROOT:-hdfs://localhost:9000/user/$(whoami)/data_in}
  local hdfs_task_root="hdfs://localhost:9000/user/$(whoami)/${class_name}/test/"
  local spark_master=local
  local start=$(date +%s)

  _build_scala && \
  spark_run_streaming_job ${SPARK_JAR_PATH} \
                          ${class_name} \
                          ${local_data_root} \
                          ${hdfs_data_root} \
                          ${hdfs_task_root} \
                          ${dynamo_name} \
                          ${spark_master}
}

function spark_run_remote() {
  local class_name="$1"
  local dynamo_name="$2"

  local master_node_ips=($(emr_configure_cluster))
  local master_node_public_ip=${master_node_ips[0]}
  local conn_string=${EMR_USER}@${master_node_public_ip}
  echo "Connection string: ${conn_string}"
  if [[ -z ${SKIP_ORCHESTRATION} ]]; then
    _build_scala && \
    scp ${SSH_OPTS} ${SPARK_JAR_PATH} ${conn_string}:${SPARK_EMR_JAR_PATH}
  fi
  ssh ${SSH_OPTS} ${conn_string} "
    set -x && \
    cd ${EMR_REPO_ROOT} && source task2.sh \
    && spark_run_cluster ${class_name} ${dynamo_name} 2>&1 | tee ${class_name}.log
  " && \
  (
    let duration=$(date +%s)-${start}
    echo "Job took ${duration} seconds"
  )
  # ccc_emr_terminate_cluster

}

# Run on driver node
function spark_run_cluster() {
  local class_name="$1"
  local dynamo_name="$2"
  local master_node_private_ip="$(wget -q -O - http://169.254.169.254/latest/meta-data/local-ipv4)"

  local hdfs_data_root="hdfs://${master_node_private_ip}:8020/${EMR_USER}/data/airline_ontime"
  local hdfs_task_root="hdfs://${master_node_private_ip}:8020/${EMR_USER}/prod/${class_name}"

  local start=$(date +%s)
  local data_mountpoint="${EMR_HOME}/airline-data"
  local emr_data_root="/${data_mountpoint}/aviation/airline_ontime"
  local spark_master=yarn

  spark_run_streaming_job \
      ${SPARK_EMR_JAR_PATH} \
      ${class_name} \
      ${emr_data_root} \
      ${hdfs_data_root} \
      ${hdfs_task_root} \
      ${dynamo_name} \
      ${spark_master} \
      '--conf spark.driver.memory=8g --conf spark.driver.maxResultSize=32g' && \
  (
    let duration=$(date +%s)-${start}
    echo "Job took ${duration} seconds"
  )

}
function spark_run_streaming_job() {
  local jar_path="$1"
  local class_name="$2"
  local local_data_root="$3"/2008
  # The place from where we copy data over
  local hdfs_data_root="$4"
  # The root over all things task related. Input data is copied there from $hdfs_data_root when job runs
  local hdfs_task_root="$5"
  local dynamo_name="$6"
  local spark_master="$7"
  local options="$8"

  local hdfs_in=${hdfs_task_root}/data_in
  local hdfs_tmp=${hdfs_task_root}/tmp

  local deploy_mode=cluster
  if [[ ${spark_master} = local ]]; then
    deploy_mode=client
  fi

  # Put data into hdfs, so that we can quickly copy it over to where
  # the streaming job will pick it up (idempotent op, assuming immutability)
  ingest ${local_data_root} ${hdfs_data_root}

  hadoop fs -rm -r ${hdfs_task_root} || true
  hadoop fs -mkdir -p ${hdfs_in}

  # start job, put into background
  spark-submit \
    --packages com.audienceproject:spark-dynamodb_2.11:0.4.1 \
    --class ${class_name} \
    --master ${spark_master} \
    --deploy-mode ${deploy_mode} \
    ${options} \
    ${jar_path} ${hdfs_in} ${hdfs_tmp} ${dynamo_name} &
  # Wait for job to initialize
  sleep 10
  # Copy data, to be picked up
  echo "$(date -Is) Copying"
  hadoop fs -cp ${hdfs_data_root}/* ${hdfs_in}
  echo "$(date -Is) Copying done"
  # Wait for spark job to finish
  wait
  # Delete the task-specific copy of the input data
  hadoop fs -rm -r ${hdfs_in}
}

function _build_scala() {
  (cd ${SCALA_PKG_ROOT}; sbt package)
}